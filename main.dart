/*
  ____                                                                  _                 
 |  _ \   _ __    ___     __ _   _ __    __ _   _ __ ___    _ __ ___   (_)  _ __     __ _ 
 | |_) | | '__|  / _ \   / _` | | '__|  / _` | | '_ ` _ \  | '_ ` _ \  | | | '_ \   / _` |
 |  __/  | |    | (_) | | (_| | | |    | (_| | | | | | | | | | | | | | | | | | | | | (_| |
 |_|     |_|     \___/   \__, | |_|     \__,_| |_| |_| |_| |_| |_| |_| |_| |_| |_|  \__, |
                         |___/                                                      |___/ 
  _       _   _                                                                           
 | |     (_) | | __   ___      __ _                                                       
 | |     | | | |/ /  / _ \    / _` |                                                      
 | |___  | | |   <  |  __/   | (_| |                                                      
 |_____| |_| |_|\_\  \___|    \__,_|                                                      
                                                                                          
  ____                                                                                    
 | __ )    ___    ___   ___                                                               
 |  _ \   / _ \  / __| / __|                                                              
 | |_) | | (_) | \__ \ \__ \                                                              
 |____/   \___/  |___/ |___/   


[v] Vscode Extentions
[v] Vscode Pro Tricks - Theme, Keyboard Shortcuts, etc...
[v] Fast Coding Writing - Tips and Tricks
[v] Creating markdown documentation
[v] git - version control - installation - part 1
[v] git - practice: new project: init, add, commit - part 2
[v] git tools - connect to gitlab - part 3
[v] git tools - clone, push, pull gitlab - part 4
[v] git tools - branches - part 5
[ ] git summary - part 6

HW:
  1. do checkout to develop
  2. add new file add_chat.dart
  3. push changes to GitLab Develop
  4. merge changes from develop to master
  5. push changes to GitLab Master

*/

// Git Installation on Windows:
// 1. environment variables
// 2. add this line: C:\Program Files\Git\bin
// 3. git init
// 4. git add .
// 5. git commit -m "new version"
// 6. add GITLAB: ssh-keygen -t ed25519 -C "<comment>"
// 7. git push -u origin master

import 'add_photo.dart';
import 'double.dart';
import 'foo.dart';
import 'number.dart';

class A {
  int x;
  A() {
    this.x = 0;
  }
}

void main() {
  //A a = A();
  //a.x
  Photo photo = Photo();
  photo.uploadPhoto();

  printNumber();
  printDouble();
}

void printDouble() {
  print(THE_DOUBLE);
}

void printNumber() {
  print(THE_NUMBER);
}

List<int> createList() {
  List<int> numbers = [1, 2, 3];
  for (int i = 0; i < 10000; i++) {
    print("${numbers[i]}");
  }
  return numbers;
}
//
